﻿using MongoDB.Bson;
namespace HelpdeskDB
{
    // Problem Class
    public class Problem
    {
        public ObjectId Id { get; set; }
        public string Description { get; set; }
    }
}
