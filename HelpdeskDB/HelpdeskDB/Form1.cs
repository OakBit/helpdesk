﻿using System;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace HelpdeskDB
{
    public partial class helpDeskForm : Form
    {
        // class level variables
        MongoCollection<Employee> employees;
        MongoCollection<Department> departments;
        MongoCollection<Problem> problems;
        MongoDatabase db;

        public helpDeskForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonCreate_Click(object sender, EventArgs e)
        // button click and remaining code, leave event handler stub alone
        {
            try
            {
                MongoClient client = new MongoClient(); // connect to localhost
                MongoServer server = client.GetServer();
                db = server.GetDatabase("HelpdeskDB");

                // remove any existing collections
                if (db.CollectionExists("departments"))
                {
                    db.DropCollection("departments");
                }
                if (db.CollectionExists("problems"))
                {
                    db.DropCollection("problems");
                }
                if (db.CollectionExists("employees"))
                {
                    db.DropCollection("employees");
                }

                // create collection infrastructure
                db.CreateCollection("departments");
                db.CreateCollection("problems");
                db.CreateCollection("employees");

                employees = db.GetCollection<Employee>("employees");
                departments = db.GetCollection<Department>("departments");
                problems = db.GetCollection<Problem>("problems");

                // load collections with hardcoded data
                LoadDepartments();
                LoadProblems();
                LoadEmployees();
                MessageBox.Show("Collections Created", "All Collections Loaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error - " + ex.Message, "All Collections Not Loaded", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadDepartments()
        {
            InsertDepartment("Administration");
            InsertDepartment("Sales");
            InsertDepartment("Food Services");
            InsertDepartment("Lab");
            InsertDepartment("Maintenance");
        }
        private void LoadProblems()
        {
            InsertProblem("Device Not Plugged In");
            InsertProblem("Device Not Turned On");
            InsertProblem("Hard Drive Failure");
            InsertProblem("Memory Failure");
            InsertProblem("Power Supply Failure");
            InsertProblem("Password fails due to Caps Lock being on");
            InsertProblem("Network Card Faulty");
            InsertProblem("Cpu Fan Failure");
            InsertProblem("Memory Upgrade");
            InsertProblem("Graphics Upgrade");
            InsertProblem("Needs software upgrade");
        }

        private void LoadEmployees()
        {
            InsertEmployee("Mr.", "Bigshot", "Smartypants", "(555) 555-5551", "bs@abc.com", "Administration");
            InsertEmployee("Mrs.", "Penny", "Pincher", "(555) 555-5551", "pp@abc.com", "Administration");
            InsertEmployee("Mr.", "Smoke", "Andmirrors", "(555) 555-5552", "sa@abc.com", "Sales");
            InsertEmployee("Mr.", "Sam", "Slick", "(555) 555-5552", "ss@abc.com", "Sales");
            InsertEmployee("Mr.", "Sloppy", "Joe", "(555) 555-5553", "sj@abc.com", "Food Services");
            InsertEmployee("Mr.", "Franken", "Beans", "(555) 555-5553", "fb@abc.com", "Food Services");
            InsertEmployee("Mr.", "Bunsen", "Burner", "(555) 555-5554", "bb@abc.com", "Lab");
            InsertEmployee("Ms.", "Petrie", "Dish", "(555) 555-5554", "pd@abc.com", "Lab");
            InsertEmployee("Ms.", "Mopn", "Glow", "(555) 555-5555", "mg@abc.com", "Maintenance");
            InsertEmployee("Mr.", "Spickn", "Span", "(555) 555-5555", "sps@abc.com", "Maintenance");
        }

        // Helper methods for inserting data into 3 collections
        private void InsertDepartment(string name)
        {
            Department dept = new Department();
            dept.DepartmentName = name;
            departments.Insert(dept);
        }
        private void InsertProblem(string description)
        {
            Problem prb = new Problem();
            prb.Description = description;
            problems.Insert(prb);
        }
        private void InsertEmployee(string title,
                                   string first,
                                   string last,
                                   string phone,
                                   string email,
                                   string dept)
        {
            Employee emp = new Employee();
            emp.Title = title;
            emp.Firstname = first;
            emp.Lastname = last;
            emp.Phoneno = phone;
            emp.Email = email;
            var query = Query<Department>.EQ(d => d.DepartmentName, dept);
            var dep = departments.FindOne(query);
            emp.DepartmentId = dep.Id;
            employees.Insert(emp);
        }

    }
}

