﻿using MongoDB.Bson;
namespace HelpdeskDB
{
    // Department Class
    public class Department
    {
        public ObjectId Id { get; set; }
        public string DepartmentName { get; set; }
    }
}
